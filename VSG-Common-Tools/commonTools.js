module.exports = function (app, config) {
    'use strict';

    const fs = require('fs');

    
    let nconf = null;
    try {
        nconf = require('nconf');

        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n ");
        console.log("nconf.get(): "+JSON.stringify(nconf.get()));
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n ");

        console.log("nconf.atg.server.admin.url: "+nconf.get('atg.server.admin.url'));
        console.log("atg.application.credentials: atg.application.token: "+nconf.get('atg.application.credentials: atg.application.token'));
        console.log("nconf.get ('atg.server.url'): "+nconf.get('atg.server.url'));

    } catch (e) {
        //console.error("LOCAL "+e);        
    }

    /**
     * get confing value from default.json config file based on key provided
     * @param  {[type]} configKey [description]
     * @return {[type]}           [description]
     */
    var getConfigValue = function (configKey) {
        if (config.has(configKey)) {
            return config.get(configKey);
        } else {
            if (config.has('environment')) {
                var environmentSpecificKey = 'environments.' + config.get('environment') + '.' + configKey;

                if (config.has(environmentSpecificKey)) {
                    return config.get(environmentSpecificKey);
                }
            }
        }
        return null;
    };


    var getValueOrEmpty = function (val, defaultVal = '') {
        var ret = defaultVal;
        if (val && typeof val === 'string') {
            return val.trim();
        }
        return ret;
    };

    /**
     * getting dynamicProperty value from passed in object.dynamicProperties and 
     * passed in dynamicProperty id
     * @param  {[type]} object [description]
     * @param  {[type]} id     [description]
     * @return {[type]}        [description]
     */
    var getValueFromObjectDynamicProperties = function (object, id) {
        var value;
        if (object && id) {
            var item = object.dynamicProperties.find(function (prop) {
                return prop.id === id;
            });
            if (item) {
                return item.value;
            }
        }
        return value;
    };



    /**
     * Statistics file. writing data , limiting to the last 100 lines of records 
     * method will create new or update existing file based on provided fileName
     * @param  {[type]} fileName [description]
     * @param  {[type]} data     [description]
     * @param  {[type]} status   [description]
     * @return {[type]}          [description]
     */
    var updateStatsFile = function (fileName, data, status, logger, limit = 5) {
        try {

            var fileJSON = ' \n  ' + new Date().toString() + ' Data: ' + JSON.stringify(data);
            if (status && status.orderId) {
                fileJSON = ' \n ' + status.orderId + ' : ' + new Date().toString() + ' Status: ' + JSON.stringify(status);
                if (status.success === false) {
                    fileJSON += ' Order Data to EBS:  ' + JSON.stringify(data);
                }
            }
            if (fs.existsSync(fileName) && limit > 0) {
                var rData = fs.readFileSync(fileName).toString().split('\n');
                var deleteCount = 0;
                if (rData && rData.length > limit) {
                    deleteCount = rData.length - limit;
                }
                rData.unshift(fileJSON);
                rData.splice(limit, deleteCount);
                fs.writeFile(fileName, rData.join('\n'), function (err) {
                    if (err) {
                        console.error('err: ', err);
                    }
                });
            } else {
                fs.writeFile(fileName, fileJSON, function (err) {
                    if (err) {
                        console.error('err: ', err);
                    }
                });
            }
        } catch (err) {
            console.error('updateStatsFile', err);
            if (logger) {
                logger.error(fileName + ". updateStatsFile error" + JSON.stringify(err));
            }
        }
    };


    var parseOCCresponse = function (response) {
        //Oracle SDK issue creates response as Buffer instead of JSON. Issue in Oracle SDK 
        // response handling and response headers content type has to match to array of valid 
        // options. Found out that there are possible variation of content type that will make response
        // as Buffer. There is a fix in Oracle SDK node_module (occrest) that will make response from SDK
        // now adding check if response is Buffer or JSON and reacting accordingly 
        if (response && response instanceof Buffer) {
            return JSON.parse(response.toString());
        } else {
            //new version with SDK fix
            return response;
        }
    };

    var getValueByPropertyName = function (object, propName) {
        try {
            var value = null;
            var nestedObj = object;
            var array = propName.split('.');
            for (var i = 0; i < array.length; i++) {
                let name = array[i];
                if (nestedObj.hasOwnProperty(name)) {
                    if (i + 1 === array.length) {
                        value = nestedObj[name];
                    } else {
                        nestedObj = nestedObj[name];
                    }
                } else {
                    break;
                }
            }
            return value;
        } catch (err) {
            return null;
        }

    };



    var getSiteConfigs = function (configs, siteId) {
        if (configs) {
            if (configs.hasOwnProperty(siteId)) {
                return configs[siteId];
            }
            // else{
            //     return Object.keys(configs)[0];
            // }
        }
        return null;
    };

    var splitStringIfAvailable = function (value, separator = ',') {
        return value ? value.split(separator) : '';
    }

    var urlParamBuilder = function (url, param, value) {
        let s = '?';
        if (url.indexOf('?') !== -1) {
            s = '&';
        }
        return url + s + param + '=' + value;
    }

    let getEnvVariable = function (varName) {
        if(nconf && nconf.get[varName]){
            return nconf.get[varName];
        }else if (process.env[varName]){
            return process.env[varName]
        }
        return '';
    }



    return {
        'getConfigValue': getConfigValue,
        'getValueFromObjectDynamicProperties': getValueFromObjectDynamicProperties,
        'getValueOrEmpty': getValueOrEmpty,
        'updateStatsFile': updateStatsFile,
        'parseOCCresponse': parseOCCresponse,
        'getValueByPropertyName': getValueByPropertyName,
        'getSiteConfigs': getSiteConfigs,
        'splitStringIfAvailable': splitStringIfAvailable,
        'urlParamBuilder': urlParamBuilder,
        'getEnvVariable': getEnvVariable
    };
};